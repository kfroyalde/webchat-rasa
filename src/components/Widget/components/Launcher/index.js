import React from 'react';
import PropTypes from 'prop-types';
import openLauncher from 'assets/launcher_button.svg';
import close from 'assets/clear-button.svg';
import Badge from './components/Badge';
import $ from 'jquery';
import './style.scss';

const Launcher = ({
  toggle,
  isChatOpen,
  badge,
  fullScreenMode,
  openLauncherImage,
  closeImage,
  bubbleImage,
  bubbleText
}) => {

  const className = ['launcher'];
  if (isChatOpen) className.push('hide-sm');
  if (fullScreenMode) className.push(`full-screen${isChatOpen ? '  hide' : ''}`);
  // console.log(bubbleImage);

  return (
    <div className={isChatOpen == false ? 'launcher-box' : 'launcher-close' }>
      {
        isChatOpen == false ?
        <div className="bubble">
          <span className="bubble-close"></span>
          <img className="bubble-avatar" src={bubbleImage} />
          <p className="bubble-desc">{ bubbleText }</p>
        </div>
        :
        null
      }
      <button
        type="button"
        className={className.join(' ')}
        onClick={toggle}
      >
        <Badge badge={badge} />
        {isChatOpen ?
          <img src={closeImage || close} className={`close-launcher ${closeImage ? '' : 'default'}`} alt="" /> :
          <img src={openLauncherImage || openLauncher} className="open-launcher" alt="" />
        }
      </button>
    </div>
  );
};

Launcher.propTypes = {
  toggle: PropTypes.func,
  isChatOpen: PropTypes.bool,
  badge: PropTypes.number,
  fullScreenMode: PropTypes.bool,
  openLauncherImage: PropTypes.string,
  closeImage: PropTypes.string,
  bubbleText: PropTypes.string,
  bubbleImage: PropTypes.string
};

export default Launcher;
