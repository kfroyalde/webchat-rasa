import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { PROP_TYPES } from 'constants';
import { addUserMessage, emitUserMessage, setQuickReply, toggleInputDisabled, changeInputFieldHint } from 'actions';
import Message from '../Message/index';

import Slider from "react-slick";

import './styles.scss';

class Snippet extends PureComponent {

  constructor(props) {
    // console.log(props)
    super(props);
    this.handleClick = this.handleClick.bind(this);

    // const hint = this.props.message.get('hint');
    // const chosenReply = this.props.getChosenReply(this.props.id);
    // if (!chosenReply && !this.props.inputState) {
    //   // this.props.toggleInputDisabled();
    //   // this.props.changeInputFieldHint(hint);
    // }
  }

  handleClick(reply) {
    const payload = reply.payload;
    const title = reply.title;
    const id = this.props.id;
    chooseReply(payload, title, id);
    // this.props.changeInputFieldHint('Type a message...');
  }

  render() {
    var settings = {
      arrows: true,
      centerMode: true,
      centerPadding: '5%',
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      adaptiveHeight: true,
      mobileFirst: true,
      responsive: [
        {
        breakpoint: 768,
        settings: {
          arrows: true,
          draggable: true,
          mobileFirst: true,
          centerMode: true,
          centerPadding: '5%',
          slidesToShow: 1
        }
        },
        {
        breakpoint: 480,
        settings: {
          arrows: true,
          draggable: true,
          mobileFirst: true,
          centerMode: true,
          centerPadding: '5%',
          slidesToShow: 1
        }
        }
      ]
    };

    // console.log(this.props.message.get('content'))
    // console.log(this.props.message.get('link'))
    // var elems = this.props.message.get('content');

    var items = this.props.message.get('content').map((a, key) =>
      <div className="content" key={key}>
        <div className="embed-responsive">
            <img src={ a.image_url } className="pjl-corp-center" />
            <p className="mb-0">
              <strong className="location-name mt-1">{ a.title }</strong>
            </p>
            <p className="subtitle">
              { a.subtitle }
            </p>
            <p className="view-map">
              <a href={ a.buttons[0].url } target="_blank">{ a.buttons[0].title }</a>
            </p>
        </div>
      </div>
    );

    // var qrb = this.props.message.get('link') ? this.props.message.get('link').map((reply, index) => 
    //       <div key={index} className={'reply'}
    //         onClick={this.handleClick.bind(this, reply)}
    //       >{reply.title}</div>
    // ) : null;

    // console.log(this.props.message.get('content'));
    return (
      // <div className="snippet">
      //   <b className="snippet-title">
      //     { this.props.message.get('title') }
      //   </b>
      //   <div className="snippet-details">
      //     <a href={this.props.message.get('link')} target={this.props.message.get('target')} className="link">
      //       { this.props.message.get('content') }
      //     </a>
      //   </div>
      // </div>
      <div className="snippet">
        <div className="center">
        <Slider {...settings}>
            { items }
        </Slider>
        </div>
      </div>

    );
  }
}

const mapStateToProps = state => ({
  getChosenReply: id => state.messages.get(id).get('chosenReply'),
  inputState: state.behavior.get('disabledInput')
});

const mapDispatchToProps = dispatch => ({
  toggleInputDisabled: _ => dispatch(toggleInputDisabled()),
  changeInputFieldHint: hint => dispatch(changeInputFieldHint(hint)),
  chooseReply: (payload, title, id) => {
    dispatch(setQuickReply(id, title));
    dispatch(addUserMessage(title));
    dispatch(emitUserMessage(payload));
    // dispatch(toggleInputDisabled());
  }
});

Snippet.propTypes = {
  message: PROP_TYPES.SNIPPET
};

export default Snippet;
