import React, { PureComponent } from 'react';
import ReactMarkdown from 'react-markdown';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { PROP_TYPES } from 'constants';
import DocViewer from '../docViewer';
import './styles.scss';
import $ from 'jquery';

class Message extends PureComponent {

  constructor(props) {
    super(props)
    // console.log(this.props.stylePayloads)
  }

  

  urlify(text) {
      var urlRegex = /(https?:\/\/[^\s]+)/g;
      return text.replace(urlRegex, function(url) {
          return '<a href="'+url+'" target="_blank">' + url + '</a>';
      })
      // or alternatively
      // return text.replace(urlRegex, '<a href="$1">$1</a>')
  }


  render() {
    // console.log(this.props)
    const { docViewer } = this.props;
    const sender = this.props.message.get('sender');
    // const text = this.urlify(this.props.message.get('text'))
    const text = this.props.message.get('text');
    const i = this.props.stylePayloads ? this.props.stylePayloads.findIndex(x => x.selected == 1) : -1;
    const styles = i != -1 ? this.props.stylePayloads[i]: { backgroundColor: '#f4f7f9', textColor: '#000000', userTextBackground: '#426591', userTextColor: '#FFFFFF' };
    
    // console.log(styles);
    return (
      <div className={sender} style={{ backgroundColor: sender == 'response' ? styles.backgroundColor : styles.userTextBackground, color: sender == 'response' ? styles.textColor : styles.userTextColor}}>
        <div className="message-text">
          {sender === 'response' ? (
          // <ReactMarkdown
          //   className={'markdown'}
          //   source={text}
          //   linkTarget={(url) => {
          //     if (!url.startsWith('mailto') && !url.startsWith('javascript')) return '_blank';
          //     return undefined;
          //   }}
          //   transformLinkUri={null}
          //   renderers={{
          //     link: props =>
          //       docViewer ? (
          //         <DocViewer src={props.href}>{props.children}</DocViewer>
          //       ) : (
          //         <a href={props.href}>{props.children}</a>
          //       )
          //   }}
          // />
          <div dangerouslySetInnerHTML={{ __html: text }} />
          ) : (
            <div dangerouslySetInnerHTML={{ __html: this.props.message.get('text') }} />
          )}
        </div>
        
      </div>
    );
  }
}

Message.propTypes = {
  message: PROP_TYPES.MESSAGE,
  docViewer: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  docViewer: state.behavior.get('docViewer')
});

export default connect(mapStateToProps)(Message);
