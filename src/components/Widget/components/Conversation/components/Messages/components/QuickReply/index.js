import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { PROP_TYPES } from 'constants';
import { addUserMessage, emitUserMessage, setQuickReply, toggleInputDisabled, changeInputFieldHint } from 'actions';
import Message from '../Message/index';

import './styles.scss';
import $ from 'jquery';

class QuickReply extends PureComponent {
  constructor(props) {
    // console.log(props)
    super(props);
    this.handleClick = this.handleClick.bind(this);

    const hint = this.props.message.get('hint');
    const chosenReply = this.props.getChosenReply(this.props.id);
    if (!chosenReply && !this.props.inputState) {
      // this.props.toggleInputDisabled();
      // this.props.changeInputFieldHint(hint);
    }
  }

  handleClick(reply) {
    const payload = reply.payload;
    const title = reply.title;
    const id = this.props.id;
    this.props.chooseReply(payload, title, id);
    
    // this.props.changeInputFieldHint('Type a message...');

    var i = this.props.stylePayloads.findIndex(x => x.payload == payload);

    if(i != -1) {
      var s = this.props.stylePayloads.findIndex(x => x.selected == 1);
      if(s != -1) {
        this.props.stylePayloads[s].selected = 0;
      }

      this.props.stylePayloads[i].selected = 1;
    }
    
  }

  render() {
    // console.log(this.props);
    const chosenReply = this.props.getChosenReply(this.props.id);
    if (chosenReply) {
      return <Message message={this.props.message} stylePayloads={this.props.stylePayloads} />
    }

    const i = this.props.stylePayloads ? this.props.stylePayloads.findIndex(x => x.selected == 1) : -1;
    const styles = i != -1 ? this.props.stylePayloads[i]: { backgroundColor: null, textColor: null };

    return (
      <div>
        <Message message={this.props.message} stylePayloads={this.props.stylePayloads} />
        {this.props.isLast &&
        <div className="replies" >
          {this.props.message.get('quick_replies').map((reply, index) => 
          <div
            key={index} className={'reply'}
            onClick={this.handleClick.bind(this, reply)}
            style={{ borderColor: styles ? styles.qrbBorderColor : null, color: styles ?  styles.qrbTextColor : null}}
          ><div dangerouslySetInnerHTML={{ __html: reply.title }} /></div>)}
        </div>
        }
      </div>);
  }
}


const mapStateToProps = state => ({
  getChosenReply: id => state.messages.get(id).get('chosenReply'),
  inputState: state.behavior.get('disabledInput')
});

const mapDispatchToProps = dispatch => ({
  toggleInputDisabled: _ => dispatch(toggleInputDisabled()),
  changeInputFieldHint: hint => dispatch(changeInputFieldHint(hint)),
  chooseReply: (payload, title, id) => {
    dispatch(setQuickReply(id, title));
    dispatch(addUserMessage(title));
    dispatch(emitUserMessage(payload));
    // dispatch(toggleInputDisabled());
  }
});

QuickReply.propTypes = {
  message: PROP_TYPES.QUICK_REPLY
};

export default connect(mapStateToProps, mapDispatchToProps)(QuickReply);
